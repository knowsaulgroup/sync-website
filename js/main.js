(function ($) {
    $( document ).ready(function() {
        $( '.menu-img' ).on( "click", function( prevent ) {
            prevent.preventDefault();

            $( '.full-screen-moible-menu' ).toggleClass( 'open');

            $( 'body' ).toggleClass( 'no-scroll' );
          });

          $('.sliding-link').click(function(e) {
              e.preventDefault();
              var dest = $(this).attr('href');
              console.log(dest);

              $('html,body').animate({
                  scrollTop: $(dest).offset().top
                }, 'slow');
            });
    });
})(jQuery);